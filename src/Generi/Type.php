<?php
namespace Generi {

    use Generi\Boundary\IStringable;
    use Generi\Boundary\IType;

	/**
     * Class Type defines type handling.
     */
    class Type implements IType, IStringable {

        public $Namespace;
        public $Name;

        private $sFullName;

        /**
         * @param $sClassName
         * @return Type
         */
        public static function getTypeOf($sClassName) {

	        $sClassName = trim($sClassName,'\\');

            $aClass = explode('\\', $sClassName);
            $sType = end($aClass);
            unset($aClass[count($aClass)-1]);

            $oType = new Type();
            $oType->Name = $sType;
            $oType->Namespace = !empty($aClass)
                ? implode('\\',$aClass)
                : '\\';

            return  $oType;
        }

	    /**
	     * Return full name of the class type along with its
	     * namespace name.
	     *
	     * @param bool $bStartsWithSlash
	     * @return string
	     */
	    public function getFullName($bStartsWithSlash = true) {

		    if($bStartsWithSlash === true) {
			    $sSlash = '\\';
		    } else {
			    $sSlash = '';
		    }

		    if(!isset($this->sFullName)) {
			    if(!$this->isInGlobalNamespace()) {
				    $this->sFullName = $sSlash.$this->Namespace.'\\'.$this->Name;
			    } else {
				    $this->sFullName = $sSlash.$this->Name;
			    }
		    }
		    return $this->sFullName;
	    }

        /**
         * Return short name of the class type or full
         * name id $bWithNamespace is true.
         *
         * @param bool $bWithNamespace
         * @return string
         */
        public function getName($bWithNamespace = false) {

            if($this->isInGlobalNamespace()) {
                return $this->Name;
            } else if($bWithNamespace == true) {
                return $this->getFullName();
            } else {
                return $this->Name;
            }

        }

        /**
         * @return ClassNamespace
         */
        public function getNamespace() {
            return new ClassNamespace($this->Namespace);
        }

        /**
         * @param $sNamespace
         * @param bool $bAppendNamespace
         * @return string
         */
        public function rewriteTypeInNamespace($sNamespace, $bAppendNamespace = true) {
            if($bAppendNamespace) {
                return $this->getNamespace()->getVendor(true) . $sNamespace . '\\'. $this->getName();
            }
            return $sNamespace . '\\'. $this->getName();
        }

        /**
         * @param $sNamespace
         * @param bool $bGetNamespaceOnly
         * @return string
         */
	    public function appendNamespace($sNamespace, $bGetNamespaceOnly = false) {
            if($bGetNamespaceOnly) {
                return $this->getNamespace()->__toString().'\\'.$sNamespace;
            }
		    return $this->getNamespace()->__toString().'\\'.$sNamespace.'\\'.$this->getName();
	    }

        /**
         * @return boolean
         */
        public function isInGlobalNamespace() {
            return $this->Namespace == '\\';
        }

        /**
         * @return string
         */
        public function __toString() {
            return $this->getFullName();
        }

    }

}

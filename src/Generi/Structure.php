<?php
namespace Generi {

	use Generi\Boundary\IToArray;
	use Generi\Boundary\IToJson;

	class Structure implements IToArray, IToJson {

		/**
		 * @var array
		 */
		private $aProperties = array();

		final public function __set($sName, $mValue) {
			$this->aProperties[$sName] = $mValue;
		}

		final public function __get($sName) {
			if (isset($this->aProperties[$sName])) {
				$mVariable = $this->aProperties[$sName];

				return $mVariable;
			}

			return null;
		}

		final public function __isset($sName) {
			return isset($this->aProperties[$sName]);
		}

		final public function __unset($sName) {
			unset($this->aProperties[$sName]);
		}

		////////////////////////////////
		// \Generi\Boundary\IToJson

		public function __toJson($bUnderscored = false) {
			return json_encode($this->__toArray($bUnderscored)->getArrayCopy());
		}

		////////////////////////////////
		// \Generi\Boundary\IToArray

		/**
		 * @param bool $bUnderscored
		 * @return \ArrayObject
		 */
		public function __toArray($bUnderscored = false) {
			$oToArray = new \ArrayObject();

			if ($bUnderscored) {
				$aProperties = $this->getUnderscoredProperties();
			} else {
				$aProperties = $this->aProperties;
			}

			foreach ($aProperties as $sKey => $mData) {
				if (is_object($mData)) {
					if ($mData instanceof IToArray) {
						$mData = $mData->__toArray($bUnderscored)->getArrayCopy();
					} else {
						$mData = 'ERROR_CouldNotSerializeToArray_No_IToArray_Interface';
					}
				}
				$oToArray[$sKey] = $mData;
			}

			return $oToArray;
		}

		/**
		 * @return array
		 */
		private function getUnderscoredProperties() {
			$aPrefixedArray = array();

			foreach ($this->aProperties as $sKey => &$sValue) {
				$aPrefixedArray[StringConverter::ConvertToUnderscore($sKey)] = &$sValue;
			}

			return $aPrefixedArray;
		}
	}
}
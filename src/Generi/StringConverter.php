<?php
namespace Generi {

	/**
	 * Converts from underscore form to camel case.
	 *
	 * @package Infrastructure
	 * @version $Id$
	 * @author Risto Kowaczewski
	 */
	class StringConverter {
		/**
		 * Convert from camel case to underscore.
		 *
		 * @param string $sCamelCasedWord
		 * @return string
		 */
		public static function ConvertToUnderscore($sCamelCasedWord) {
			return strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', $sCamelCasedWord));
		}

		/**
		 * Convert from underscore to camel case.
		 *
		 * @param string $sUnderscoredWord
		 * @return string
		 */
		public static function ConvertToCamelCase($sUnderscoredWord) {
			return str_replace(' ', '', ucwords(str_replace('_', ' ', $sUnderscoredWord)));
		}

		/**
		 * Convert from underscore to space separated.
		 *
		 * @param string $sUnderscoredWord
		 * @return string
		 */
		public static function SpaceSeparated($sUnderscoredWord) {
			return str_replace('_', ' ', ucwords($sUnderscoredWord));
		}
	}
}


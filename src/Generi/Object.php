<?php
namespace Generi {

    use Generi\Boundary\IObject;

	class Object implements IObject {
        /**
         * @var \Generi\Type
         */
        private $oType;

        /**
         * Returns type of an object
         *
         * @return \Generi\Type
         */
        final public function getType() {

            if (!isset($this->oType)) {
                $aClass = explode('\\', get_class($this));
                $sType = end($aClass);
                unset($aClass[count($aClass) - 1]);

                $oType = new Type();
                $oType->Name = $sType;
                $oType->Namespace = !empty($aClass)
                    ? implode('\\', $aClass)
                    : '\\';

                $this->oType = $oType;
            }

            return $this->oType;
        }

        /**
         * @return \Generi\Type
         */
        final static function Type() {
            return Type::getTypeOf(get_called_class());
        }

        /**
         * @return string
         */
        final public function __toHash() {
            return spl_object_hash($this);
        }

    }

}



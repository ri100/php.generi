<?php
namespace Generi {

    use Closure;
    use Generi\Boundary\ICountableIterator;

    abstract class CountableIterator implements ICountableIterator {

        /**
         * @var \Closure
         */
        protected $pFilterFunction;

        /**
         * @return array
         */
        abstract protected function currentItemAsArray();

        /**
         * @return bool
         */
        abstract protected function validItem();

        /**
         * Example:
         *
         * $o = new \Ormianin\Result(Database::getInstance()->query($s));
         * $o->filterBy(
         *        function(array $aItem) {
         *            return $aItem['Article::Id'] == 765 ;
         *        }
         * )->castTo(new Article());
         *
         * Filter can be reset by NULL:
         *
         * $o->filterBy( null );
         *
         * @param Closure $pFilterFunction
         * @return $this
         */
        public function filterBy(Closure $pFilterFunction = null) {
            $this->pFilterFunction = $pFilterFunction;
            return $this;
        }

        ///////////////////////////////
        // Iterator

        /**
         * @return mixed
         */
        public function current() {
            return $this->currentItemAsArray();
        }

        /**
         * @see Iterator::valid()
         */
        public function valid() {
            if ($this->validItem()) {
                if ($this->filter()) {
                    return true;
                }

                $this->next();
                return $this->valid();
            }

            return false;
        }

        /**
         * @param \Iterator $oCollection
         * @param $sIndexName
         * @return array
         */
        public static function getIndexedBy(\Iterator $oCollection, $sIndexName) {
            $aIndex = array();
            foreach ($oCollection as $aItem) {
                if (isset($aItem[$sIndexName])) {
                    $aIndex[$aItem[$sIndexName]] = $aItem;
                }
            }

            return $aIndex;
        }

        //////////////////////////////////

        /**
         * @return bool
         * @throws CollectionFilterException
         */
        private function filter() {

            if (isset($this->pFilterFunction)) {

                $aItem = $this->current();

                $pFunction = $this->pFilterFunction;
                $bResult = $pFunction($aItem);

                if (!is_bool($bResult)) {
                    if (is_object($bResult)) {
                        $bResult = 'type ' . get_class($bResult);
                    } else if (is_null($bResult)) {
                        $bResult = 'NULL';
                    }
                    throw new CollectionFilterException('Function set by filterBy method must return True or False. Value of [' . $bResult . '] returned!');
                }

                if (!$bResult) {
                    return false;
                }
            }

            return true;
        }
    }
}
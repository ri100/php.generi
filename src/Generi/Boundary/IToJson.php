<?php
namespace Generi\Boundary {
    interface IToJson {
        /**
         * Returns data to be serialized to json.
         *
         * @return mixed
         */
        public function __toJson();
    }
}
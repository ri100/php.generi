<?php
namespace Generi\Boundary {
    interface IConstrainValueType {
        /**
         * Return array of propertyName => Type to constrain property to class type.
         *
         * Example:
         *
         * return array( 'property' => '\stdClass' )
         *
         * @return array
         */
        public function __constrainSpec();
    }
}



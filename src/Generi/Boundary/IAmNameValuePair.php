<?php
namespace Generi\Boundary;

interface IAmNameValuePair extends IAmNameValuePairImmutable
{
    public function setValue($sValue);
    public function setName($sName);
}
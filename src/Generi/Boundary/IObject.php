<?php
namespace Generi\Boundary {

	interface IObject {

        /**
         * Returns type of an object
         *
         * @return \Generi\Type
         */
        public function getType();

        /**
         * @return string
         */
        public function __toHash();

    }

}

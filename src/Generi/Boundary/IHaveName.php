<?php
namespace Generi\Boundary {

	interface IHaveName {

        /**
         * @return string
         */
        public function getName();

        /**
         * @return boolean
         */
        public function hasName();

    }

}
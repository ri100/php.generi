<?php
namespace Generi\Boundary {

    interface IToArray {
        /**
         * @return \ArrayObject
         */
        public function __toArray();
    }

}
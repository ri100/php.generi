<?php
namespace Generi\Boundary;

interface IEntity {

	public function setId($iId);

	/**
	 * @return int
	 */
	public function getId();

	/**
	 * @return bool
	 * @Invariant('type'='bool')
	 */
	public function hasId();
}

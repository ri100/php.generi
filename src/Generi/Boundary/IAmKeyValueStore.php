<?php
namespace Generi\Boundary {

	interface IAmKeyValueStore {

		/**
		 * @param $sKey
		 * @return mixed | null
		 */
		public function get($sKey);

		public function set($sKey, $mValue);

		/**
		 * @param $sKey
		 */
		public function remove($sKey);

		/**
		 * @param $sKey
		 * @return bool
		 */
		public function has($sKey);

	}

}

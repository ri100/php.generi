<?php
namespace Generi\Boundary {

    interface ICollection extends ICountableIterator, \ArrayAccess, IKeyValueCollection, IToArray {

	    public function hasValue($sValue);
	    public function merge(ICollection $oCollection);
	    public function filterBy(\Closure $pFilterFunction = null);
	    public function iterate(\Closure $pFunctionForItem = null);
	    public function __toJson();
	    public function __toString();

    }
}



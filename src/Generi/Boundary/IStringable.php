<?php
namespace Generi\Boundary {

	interface IStringable {
        /**
         * @return string
         */
        public function __toString();

    }

}
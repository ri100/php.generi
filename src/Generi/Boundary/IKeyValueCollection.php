<?php
namespace Generi\Boundary;

/**
 * Implement this interface if you want to have separated access to class properties.
 *
 * @verison $Id$
 */
interface IKeyValueCollection {

    /**
     * Returns TRUE if has value at given key.
     *
     * @param string $sKey
     * @return boolean
     */
    public function has($sKey);

    /**
     * Removes value by key.
     *
     * @param string | array $mKey
     * @return void
     */
    public function remove($mKey);

    /**
     * Removes items from collection other then defined in $aKeys param.
     *
     * @param array $aKeys
     * @return void
     */
    public function allowOnly(array $aKeys);

    /**
     * Reads value.
     *
     * @param string $sKey
     * @return mixed
     */
    public function get($sKey);

    /**
     * Adds value.
     *
     * @param string $sKey
     * @param mixed $mValue
     * @return void
     */
    public function add($mValue, $sKey = null);

    /**
     * Return all key, value pairs.
     *
     * @return array
     */
    public function getAll();

    /**
     * @param array $aData
     */
    public function addAll($aData);

    /**
     * @return void
     */
    public function removeAll();

    /**
     * @return bool
     */
    public function isEmpty();

    /**
     * @return void
     */
    public function isSingle();
}
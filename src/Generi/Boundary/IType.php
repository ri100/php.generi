<?php
namespace Generi\Boundary {

    interface IType {
        public function getFullName();
        public function getName($bWithNamespace = false);
        public function getNamespace();
        public function rewriteTypeInNamespace($sNamespace);
        public function isInGlobalNamespace();
    }

}
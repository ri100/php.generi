<?php
namespace Generi\Boundary {

	/**
	 * Implement on FormElements.
	 */
	interface IValuable {
		/**
		 * Return TRUE if value is not empty.
		 *
		 * @return bool
		 */
		public function isEmpty();

		/**
		 * Sets value for IValuable objects. Should be able to take array or string.
		 * Array values are for indexed FormElements. It can be passed from request.
		 *
		 * @param mixed
		 */
		public function setValue($sValue);

		/**
		 * Returns value from IValuable object.
		 *
		 * @return mixed
		 */
		public function getValue();
	}

}

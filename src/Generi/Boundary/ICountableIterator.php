<?php
namespace Generi\Boundary {

    interface ICountableIterator extends \Iterator, \Countable {

    }

}
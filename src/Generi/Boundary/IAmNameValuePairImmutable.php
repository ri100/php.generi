<?php
namespace Generi\Boundary;

interface IAmNameValuePairImmutable
{
    public function getValue();
    public function getName();
    public function hasValue($sValue);
}
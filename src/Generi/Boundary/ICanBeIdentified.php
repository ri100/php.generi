<?php
namespace Generi\Boundary {

	interface ICanBeIdentified extends IHaveName {
		/**
		 * @return \Generi\Type
		 */
		public function getType();
	}
    
}


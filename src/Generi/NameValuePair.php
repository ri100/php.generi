<?php
namespace Generi {

    class NameValuePair extends Object {

        private $sName;
        private $sValue;

        /**
         *
         * @return string $sKey
         */
        public function getName() {
            return $this->sName;
        }
        /**
         *
         * @return string $sValue
         */
        public function getValue() {
            return $this->sValue;
        }

        /**
         *
         * @param string $sName
         * @param string $sValue
         * @throws Exception
         */
        public function __construct($sName, $sValue = null) {

            if(!is_string($sName)) {
                throw new Exception('Session key must be string');
            }

            $this->sValue = $sValue;
            $this->sName = $sName;

        }

        public function __toString() {
            if(!$this->hasValue()) {
                $sValue = '';
            } else {
                $sValue = $this->getValue();
            }
            return $this->getName() . '=' . $sValue;
        }

        /**
         * Return TRUE if value is not empty.
         *
         * @return bool
         */
        public function isEmpty() {
            return empty($this->sValue);
        }

        /**
         * @return bool
         */
        public function hasValue() {
            return isset($this->sValue);
        }

        public function remove() {
            unset($this->sValue);
        }

    }

}

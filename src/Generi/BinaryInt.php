<?php
namespace Generi {

	class BinaryInt {

		private $iInt;

		public function __construct($iInt) {
			$this->setValue($iInt);
		}

		/**
		 * @param int
		 * @return void
		 */
		public function setValue($iInt) {
			$this->iInt = (int)$iInt;
		}

		/**
		 * @param int
		 * @return int
		 */
		public function getValue() {
			return $this->iInt;
		}

		/**
		 * @param $iBit
		 * @throws \Exception
		 * @return bool
		 * @Invariant('type'='bool')
		 */
		public function hasBit($iBit) {
			if (is_int($iBit)) {
				return ($this->iInt & $iBit) == $iBit;
			}
			throw new \Exception('Bit must be at int.');
		}
	}
}

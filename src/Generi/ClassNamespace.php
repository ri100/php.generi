<?php
namespace Generi {

	/**
     * Class Generi\ClassNamespace
     *
     * Keeps information of class namespace.
     */
    class ClassNamespace {

        private $sNamespace;
        private $sVendor;

        public function __construct($sNamespace) {
            if(strpos($sNamespace, '\\') === 0) {
                $this->sNamespace = substr($sNamespace, 1);
            } else {
                $this->sNamespace = $sNamespace;
            }
        }

        /**
         * @return bool
         */
        public function isVendorNotEmpty() {
            $sVendor = $this->getVendor();
            return !empty($sVendor);
        }

        /**
         * @param bool $bPrefixWithSlash
         * @return string
         */
        public function getVendor($bPrefixWithSlash = false) {
            if(!isset($this->sVendor)) {
                if(strpos($this->sNamespace, '\\') === 0) {
                    $this->sNamespace = trim(substr($this->sNamespace, 1));
                }

                if(empty($this->sNamespace)) {
                    return '';
                }

                $aParts = explode('\\', $this->sNamespace);

                if(count($aParts) <= 0) {
                    return '';
                }

                $this->sVendor = $aParts[0];
            }

            if($bPrefixWithSlash) {
                return '\\'.$this->sVendor;
            }
            return $this->sVendor;
        }

        public function __toString() {
            return $this->sNamespace;
        }

    }

}

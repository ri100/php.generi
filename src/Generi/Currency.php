<?php
namespace Generi {

	class Currency {
		const PLN = 'PLN';
		const EUR = 'EURO';
		const USD = 'USD';
	}

}

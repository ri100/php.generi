<?php
namespace Generi {

	class Money {
		/**
		 * @var double
		 */
		private $iValue;
		/**
		 *
		 * @var string
		 */
		private $sCurrency;

		private $bShortValue = false;

		public function __construct($iValue, $iCurrency = null, $bShortValue = false) {
			$this->bShortValue = $bShortValue;
			$this->iValue = (!is_null($iValue)) ? $iValue : 0;
			if (!is_null($iCurrency)) {
				$this->sCurrency = $iCurrency;
			} else {
                $this->sCurrency = Currency::PLN;
            }
		}

		public function getValue() {
			return $this->iValue;
		}

		public function getCurrency() {
			return $this->sCurrency;
		}

		public function setShortValue($bShortValue) {
			$this->bShortValue = $bShortValue;
		}

		public function getValueFormated() {
			if ($this->sCurrency == Currency::PLN) {
				return number_format($this->iValue, 2, ',', ' ');
			} else if ($this->sCurrency == Currency::USD) {
				return '$' . number_format($this->iValue, 2, '.', ',');
			} else {
				return '&euro;' . number_format($this->iValue, 2, ',', ',');
			}
		}

		public function __toString() {

			if ($this->sCurrency == Currency::PLN) {
				$iValue = $this->iValue;
				$sBigAmount = '';
				if ($this->bShortValue) {
					if ($this->iValue >= 1000000000000000000 || $this->iValue <= -1000000000000000000) {
						$iValue = $this->iValue / 1000000000000000;
						$sBigAmount = ' tryl.';
					} else if ($this->iValue >= 1000000000000000 || $this->iValue <= -1000000000000000) {
						$iValue = $this->iValue / 1000000000000;
						$sBigAmount = ' bill.';
					} else if ($this->iValue >= 1000000000000 || $this->iValue <= -1000000000000) {
						$iValue = $this->iValue / 1000000000;
						$sBigAmount = ' bln.';
					} else if ($this->iValue >= 1000000000 || $this->iValue <= -1000000000) {
						$iValue = $this->iValue / 1000000000;
						$sBigAmount = ' mld.';
					} else if ($this->iValue >= 1000000 || $this->iValue <= -1000000) {
						$iValue = $this->iValue / 1000000;
						$sBigAmount = ' mln.';
					} else if ($this->iValue >= 1000 || $this->iValue <= -1000) {
						$iValue = $this->iValue / 1000;
						$sBigAmount = ' tys.';
					}
				}
				return number_format(doubleval($iValue), 2, ',', ' ') . '<span class="Currency">' . $sBigAmount . ' <sup>zł</sup></span>';
			} else if ($this->sCurrency == Currency::USD) {
				return '$' . number_format($this->iValue, 2, '.', ',');
			} else if ($this->sCurrency == Currency::EUR) {
				return '&euro;' . number_format($this->iValue, 2, ',', ',');
			}
			return $this->sCurrency . ' ' . $this->iValue;
		}
	}
}
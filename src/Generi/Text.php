<?php
namespace Generi {

	use Generi\Boundary\IStringable;

	class Text extends Object implements IStringable {

		private $sString;

		public function __construct($sString) {

			if ($sString instanceof IStringable) {
				$sString = $sString->__toString();
			}

			if (!is_string($sString)) {
				throw new Exception('Variable passed to ' . $this->getType()->getFullName() . ' object is not string.');
			}

			$this->sString = $sString;
		}

		public function equal($sString) {
			return $this->sString == $sString;
		}

		/**
		 * @return $this
		 */
		public function toUnderscore() {
			$this->sString = StringConverter::ConvertToUnderscore($this->sString);
			return $this;
		}

		/**
		 * @return $this
		 */
		public function toCamelCase() {
			$this->sString = StringConverter::ConvertToCamelCase($this->sString);
			return $this;
		}

		/**
		 * Convert from underscore to space separated.
		 *
		 * @return $this
		 */
		public function toSpaceSeparated() {
			$this->sString = StringConverter::SpaceSeparated($this->sString);
			return $this;
		}

		/**
		 * Make a string uppercase.
		 *
		 * @return $this the upper cased string.
		 */
		public function toUpperCase() {
			$this->sString = strtoupper($this->sString);
			return $this;
		}

		/**
		 * @return $this
		 */
		public function toLowerCase() {
			$this->sString = strtolower($this->sString);
			return $this;
		}

		/**
		 * @param $sNeedle
		 * @return \String
		 */
		public function find($sNeedle) {
			return new Text(strstr($this->sString, $sNeedle));
		}

		/**
		 * @param $sNeedle
		 * @return bool
		 */
		public function has($sNeedle) {
			return false !== strstr($this->sString, $sNeedle);
		}

		/**
		 * @param $iStart
		 * @param $iLength
		 * @return \Generi\Text
		 */
		public function subString($iStart, $iLength = null) {
            if(null == $iLength) {
                $String = substr($this->sString, $iStart);
            } else {
                $String = substr($this->sString, $iStart, $iLength);
            }
			return new Text($String);
		}

		/**
		 * @param $sFind
		 * @param $sReplace
		 * @return String
		 */
		public function replace($sFind, $sReplace) {
			return new Text(str_replace($sFind, $sReplace, $this->sString));
		}

		/**
		 * @return int
		 */
		public function length() {
			return strlen($this->sString);
		}

		/**
		 * @param $sString
		 * @return bool
		 */
		public function startsWith($sString) {
			return $sString == substr($this->sString, 0, strlen($sString));
		}

		/**
		 * Removes prefix
		 *
		 * @param $sString
		 * @return $this | bool
		 */
		public function dePrefix($sString) {
			if ($this->startsWith($sString)) {
				return $this->subString(strlen($sString));
			}

			return false;
		}

		/**
		 * @param $sString
		 * @return $this
		 */
		public function prefix($sString) {
			$this->sString = $sString . $this->sString;
			return $this;
		}

		/**
		 * @return string
		 */
		public function __toString() {
			return is_null($this->sString) ? '' : $this->sString;
		}

	}

}
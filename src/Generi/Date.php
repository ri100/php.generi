<?php
namespace Generi;

class Date {

	private $tDate;

	public function __construct($mDate = null) {
		if(is_null($mDate)) {
			$mDate = time();
		}
        if(is_string($mDate)) {
            $mDate = strtotime($mDate);
        }
		$this->tDate = $mDate;
	}

	public function getTimeStamp() {
		return $this->tDate;
	}

	public function __toString() {
		return date('Y-m-d H:i:s',$this->tDate);
	}

	public static function getDateOnly($tDate) {
		return date('Y/m/d',strtotime($tDate));
	}
	
	public function getSecondsTo(Date $oDate) {
		$iTimeStamp1 = $oDate->getTimeStamp();
		$iTimeStamp2 = $this->getTimeStamp();

		return $iTimeStamp1 - $iTimeStamp2;
	}
	
	public function isLaterThen(Date $oDate) {
		return $this->tDate > $oDate->getTimeStamp();
	}
}

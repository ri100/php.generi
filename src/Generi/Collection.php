<?php
namespace Generi;

use ArrayObject;

/**
 * Class Collection handles all collections.
 */
class Collection extends CountableIterator implements Boundary\ICollection {

    /**
     * @var array
     */
    private $aItems = array();

    /**
     * @param array $aData
     */
    public function __construct(array &$aData = null) {
        $this->setStorage($aData);
    }

    /**
     * @return array
     */
    protected function &getStorage() {
        return $this->aItems;
    }

    /**
     * @param null $aData
     * @throws \Exception
     */
    protected function setStorage(&$aData = null) {
        if (is_null($aData)) {
            $this->aItems = array();
        } else {
            if (is_array($aData)) {
                $this->aItems =& $aData;
            } else {
                throw new \Exception('Collection can be set up form array only.');
            }
        }
    }

    /**
     * @param $sValue
     * @return bool
     */
    public function hasValue($sValue) {
        return in_array($sValue, $this->aItems);
    }

    ////////////////////////////
    // \IToArray

    /**
     * @return ArrayObject
     */
    public function __toArray() {

        $aToJson = new ArrayObject();

        $this->iterate(function ($sKey, $mItem) use ($aToJson) {
            if (is_object($mItem)) {
                if ($mItem instanceof Boundary\IToArray) {
                    $mItem = $mItem->__toArray()->getArrayCopy();
                } else {
                    $mItem = 'ERROR_CouldNotSerializeToArray_No_IToArray_Interface';
                }
            }
            $aToJson[$sKey] = $mItem;
        });

        $this->iterate();

        return $aToJson;
    }

    ////////////////////////////
    // \IToJson

    public function __toJson() {
        return json_encode($this->__toArray()->getArrayCopy());
    }


    ///////////////////////////
    // IKeyValueCollection

    /**
     * @param string $sKey
     * @param bool $aDisallowEmpty
     * @return bool
     * @throws \Exception
     */
    public function has($sKey, $aDisallowEmpty = false) {

        if (is_object($sKey)) {
            /** @var $sKey object */
            throw new \Exception('Collection key is of type [' . get_class($sKey) . ']. Collection key can not be object.');
        }

        if(!is_array($this->getStorage())) {
            throw new \Exception('Collection storage is not array.');
        }

        if (is_array($sKey)) {

            if (empty($sKey)) {
                throw new \Exception('Key can not be empty.');
            }

            $aStorage = $this->getStorage();
            foreach ($sKey as $sKeyToCheck) {
	            if (!array_key_exists($sKeyToCheck, $aStorage ) || ( $aDisallowEmpty && empty($aStorage[$sKeyToCheck]))) {
		            return false;
	            }
            }
            return true;
        }

        return array_key_exists($sKey, $this->getStorage());
    }

    /**
     * @param mixed $sKey
     * @return mixed
     */
    public function get($sKey) {

        $aStorage =& $this->getStorage();

        if ($this->has($sKey)) {
            if (is_array($sKey)) {
                $aParams = array();
                foreach ($sKey as $sKeyToCheck) {
                    if (array_key_exists($sKeyToCheck, $aStorage)) {
                        $aParams[$sKeyToCheck] = $aStorage[$sKeyToCheck];
                    }
                }
                return $aParams;
            }

            return $aStorage[$sKey];
        }
        return null;
    }

    /**
     * @param mixed $oItem
     * @param null $sKey
     * @return $this
     */
    public function add($oItem, $sKey = null) {

        $aStorage =& $this->getStorage();

        if (is_null($sKey)) {
            $aStorage[] = $oItem;
        } else {
            $aStorage[$sKey] = $oItem;
        }

        return $this;
    }

    /**
     * @param array|string $mKey
     * @return $this
     */
    public function remove($mKey) {
        $aStorage =& $this->getStorage();

        if (is_array($mKey)) {
            foreach ($mKey as $sKeyValue) {
                unset($aStorage[$sKeyValue]);
            }
        } else {
            unset($aStorage[$mKey]);
        }

        return $this;
    }

    /**
     * @param array $aKeys
     */
    public function allowOnly(array $aKeys) {
        $aStorage =& $this->getStorage();

        foreach ($aStorage as $sKey => $sValue) {
            if (!in_array($sKey, $aKeys)) {
                unset($aStorage[$sKey]);
            }
        }
    }

    /**
     * @return array
     */
    public function &getAll() {
        return $this->getStorage();
    }

    /**
     * @param array $aData
     */
    public function addAll($aData) {
        $this->setStorage($aData);
    }

    /**
     * @return void
     */
    public function removeAll() {
        $aStorage =& $this->getStorage();
        unset($aStorage);
        $this->setStorage();
    }

    /**
     * @return bool
     */
    public function isEmpty() {
        $aStorage = $this->getStorage();
        return empty($aStorage);
    }

    /**
     * @return bool
     */
    public function isSingle() {
        return count($this->getStorage()) == 1;
    }

    public function merge(Boundary\ICollection $oCollection) {
        $this->aItems = array_merge($this->getAll(), $oCollection->getAll());
    }

    ///////////////////////////
    // Iterator

    protected function currentItemAsArray() {
        return current($this->getStorage());
    }

    protected function validItem() {
        return key($this->getStorage()) !== null;
    }

    public function rewind() {
        reset($this->getStorage());
    }

    public function key() {
        return key($this->getStorage());
    }

    public function next() {
        next($this->getStorage());
    }

    public function __toString() {
        return implode("\n", $this->getAll());
    }

    /**
     * @return int
     */
    public function count() {
        return count($this->getAll());
    }

    //////////////////////
    // \ArrayAccess

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset) {
        return $this->has($offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset) {
        return $this->get($offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value) {
        $this->add($value, $offset);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset) {
        $this->remove($offset);
    }

    /**
     *
     * Iterated collection. If Cosure is set then it will be invoked before
     * each item.
     *
     * If Closure returns not null value it will be set as
     * item value;
     *
     * @param callable $pFunctionForItem
     */
    public function iterate(\Closure $pFunctionForItem = null) {
        foreach ($this as $sKey => $mItem) {
            if(!is_null($pFunctionForItem)) {
                $mReturn = $pFunctionForItem($sKey, $mItem);
                // Set if return is not null
                if(!is_null($mReturn)) {
                    $this[$sKey] = $mReturn;
                }
            }
        }
    }

}
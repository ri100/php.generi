<?php
namespace Generi {

    use Generi\Boundary\IConstrainValueType;

    class ReadOnlyDataObject extends Object {

        private $aValues = array();

        /**
         * @param $sName
         * @param $sValue
         * @throws Exception
         */
        protected function __set($sName, $sValue) {
            if($this instanceof IConstrainValueType){
                $aConstrainSpec = $this->__constrainSpec();
                if(isset($aConstrainSpec[$sName])) {
                    $sType = $aConstrainSpec[$sName];
                    if(!$sValue instanceof $sType) {
                        throw new Exception(sprintf('Property [%s] must be of type [%s].', $sName, $sType));
                    }
                }
            }

            $this->aValues[$sName] = $sValue;
        }

        /**
         * @param $sName
         * @return mixed
         */
        public function __get($sName) {
            return $this->aValues[$sName];
        }

        /**
         * @param $sName
         * @return bool
         */
        public function __isset($sName) {
            return isset($this->aValues[$sName]);
        }

        /**
         * @param $sName
         */
        protected function __unset($sName) {
            unset($this->aValues[$sName]);
        }
    }

}

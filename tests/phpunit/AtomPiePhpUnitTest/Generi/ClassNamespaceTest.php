<?php

namespace Vendor {

    use Generi\Object;

	class ClassXInVendorNamespace extends Object {

	}
}

namespace {

    use Generi\Object;

    class ClassYInGlobalNamespace extends Object {

	}
}

namespace AtomPiePhpUnitTest\Generi {

	use Generi\ClassNamespace;
    use Vendor\ClassXInVendorNamespace;

    /**
	 * Collection test case.
	 */
	class ClassNamespaceTest extends \PHPUnit_Framework_TestCase {

		/**
		 * Prepares the environment before running a test.
		 */
		protected function setUp() {
			parent::setUp();
		}

		/**
		 * Cleans up the environment after running a test.
		 */
		protected function tearDown() {
			parent::tearDown();
		}

		public function testGetVendor() {

			$sNamespace = '\\Vendor\\Namespace\\Class';
			$oNamespace = new ClassNamespace($sNamespace);
			$this->assertTrue($oNamespace->getVendor(true) == '\Vendor');
			$this->assertTrue($oNamespace->getVendor() == 'Vendor');

			$sNamespace = '\\Vendor\\Class';
			$oNamespace = new ClassNamespace($sNamespace);
			$this->assertTrue($oNamespace->getVendor() == 'Vendor');

			$sNamespace = '\\Vendor';
			$oNamespace = new ClassNamespace($sNamespace);
			$this->assertTrue($oNamespace->isVendorNotEmpty());
			$this->assertTrue($oNamespace->getVendor() == 'Vendor');

			$sNamespace = '\\';
			$oNamespace = new ClassNamespace($sNamespace);
			$this->assertTrue(!$oNamespace->isVendorNotEmpty());
			$this->assertTrue($oNamespace->getVendor() == '');


			$oX = new ClassXInVendorNamespace();
			$this->assertTrue($oX->getType()->getNamespace()->isVendorNotEmpty());
			$this->assertTrue($oX->getType()->getNamespace()->getVendor() == 'Vendor');

			$oY = new \ClassYInGlobalNamespace();
			$this->assertTrue(!$oY->getType()->getNamespace()->isVendorNotEmpty());
			$this->assertTrue($oY->getType()->getNamespace()->getVendor() == '');

		}

		public function testRenderWithSlash() {

			$sNamespace = '\\Vendor\\Namespace\\Class';
			$oNamespace = new ClassNamespace($sNamespace);

			$this->assertTrue($oNamespace->__toString() == 'Vendor\\Namespace\\Class');

			$sNamespace = 'Vendor\\Class';
			$oNamespace = new ClassNamespace($sNamespace);
			$this->assertTrue($oNamespace->__toString() == 'Vendor\\Class');

			$sNamespace = '\\Vendor\\Class';
			$oNamespace = new ClassNamespace($sNamespace);
			$this->assertTrue($oNamespace->__toString() == 'Vendor\\Class');

		}

        public function testNamespace_EmptyNamespace() {
            $sNamespace = 'Class';
            $oNamespace = new ClassNamespace($sNamespace);
            $this->assertTrue($oNamespace->getVendor() == 'Class');
        }
	}
}


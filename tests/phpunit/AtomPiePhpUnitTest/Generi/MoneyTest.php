<?php
namespace AtomPiePhpUnitTest\Generi;

use Generi\Currency;
use Generi\Money;

class MoneyTest extends \PHPUnit_Framework_TestCase {
    public function testMoney() {
        $oMoney = new Money(100, Currency::PLN);
        $this->assertTrue($oMoney->__toString() == "100,00<span class=\"Currency\"> <sup>zł</sup></span>");

        $oMoney = new Money(100.23, "CZK");
        $this->assertTrue($oMoney->__toString() == "CZK 100.23");

    }

    public function testMoney_getValues() {
        $oMoney = new Money(100.23, Currency::PLN);
        $this->assertTrue($oMoney->getValue() == 100.23);
        $this->assertTrue($oMoney->getCurrency() == Currency::PLN);
        $this->assertTrue($oMoney->getValueFormated() == '100,23');
    }

    public function testMoney_Currency() {

        $oMoney = new Money(10.23, Currency::EUR);
        $this->assertTrue($oMoney->getValue() == 10.23);
        $this->assertTrue($oMoney->getCurrency() == Currency::EUR);
        $this->assertTrue($oMoney->getValueFormated() == '&euro;10,23');
        $this->assertTrue($oMoney->__toString() == '&euro;10,23');

        $oMoney = new Money(10012.23, Currency::EUR);
        $this->assertTrue($oMoney->getValue() == 10012.23);
        $this->assertTrue($oMoney->getCurrency() == Currency::EUR);
        $this->assertTrue($oMoney->getValueFormated() == '&euro;10,012,23');

        $oMoney = new Money(10012345.23, Currency::EUR);
        $this->assertTrue($oMoney->getValue() == 10012345.23);
        $this->assertTrue($oMoney->getCurrency() == Currency::EUR);
        $this->assertTrue($oMoney->getValueFormated() == '&euro;10,012,345,23');

        $oMoney = new Money(10012345678.23, Currency::EUR);
        $this->assertTrue($oMoney->getValue() == 10012345678.23);
        $this->assertTrue($oMoney->getCurrency() == Currency::EUR);
        $this->assertTrue($oMoney->getValueFormated() == '&euro;10,012,345,678,23');

        $oMoney = new Money(10012345678.23, Currency::USD);
        $this->assertTrue($oMoney->getValue() == 10012345678.23);
        $this->assertTrue($oMoney->getCurrency() == Currency::USD);
        $this->assertTrue($oMoney->getValueFormated() == '$10,012,345,678.23');
        $this->assertTrue($oMoney->__toString() == '$10,012,345,678.23');
    }

    public function testMoney_ShortValue() {

        $oMoney = new Money(10.23);
        $oMoney->setShortValue(true);
        $this->assertTrue($oMoney->getCurrency() == Currency::PLN);
        $this->assertTrue($oMoney->__toString() == "10,23<span class=\"Currency\"> <sup>zł</sup></span>");

        $oMoney = new Money(10012.23);
        $oMoney->setShortValue(true);
        $this->assertTrue($oMoney->getCurrency() == Currency::PLN);
        $this->assertTrue($oMoney->__toString() == "10,01<span class=\"Currency\"> tys. <sup>zł</sup></span>");

        $oMoney = new Money(10012345.23);
        $oMoney->setShortValue(true);
        $this->assertTrue($oMoney->getCurrency() == Currency::PLN);
        $this->assertTrue($oMoney->__toString() == "10,01<span class=\"Currency\"> mln. <sup>zł</sup></span>");

        $oMoney = new Money(10012345678.23);
        $oMoney->setShortValue(true);
        $this->assertTrue($oMoney->getCurrency() == Currency::PLN);
        $this->assertTrue($oMoney->__toString() == "10,01<span class=\"Currency\"> mld. <sup>zł</sup></span>");

        $oMoney = new Money(10012345678000.23);
        $oMoney->setShortValue(true);
        $this->assertTrue($oMoney->getCurrency() == Currency::PLN);
        $this->assertTrue($oMoney->__toString() == "10 012,35<span class=\"Currency\"> bln. <sup>zł</sup></span>");

        $oMoney = new Money(10012345678000000.23);
        $oMoney->setShortValue(true);
        $this->assertTrue($oMoney->getCurrency() == Currency::PLN);
        $this->assertTrue($oMoney->__toString() == "10 012,35<span class=\"Currency\"> bill. <sup>zł</sup></span>");

        $oMoney = new Money(10012345678000000000.23);
        $oMoney->setShortValue(true);
        $this->assertTrue($oMoney->getCurrency() == Currency::PLN);
        $this->assertTrue($oMoney->__toString() == "10 012,35<span class=\"Currency\"> tryl. <sup>zł</sup></span>");
    }
}

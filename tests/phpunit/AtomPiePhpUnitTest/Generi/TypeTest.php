<?php
namespace AtomPiePhpUnitTest\Generi {

    use Generi\Type;

    class TypeTest extends \PHPUnit_Framework_TestCase {
        public function testType() {
            $oType = Type::getTypeOf('Vendor\Namespace\Class');
            $this->assertTrue($oType->getFullName() == '\Vendor\Namespace\Class');
            $this->assertTrue($oType->__toString() == '\Vendor\Namespace\Class');
            $this->assertTrue($oType->getName() == 'Class');
            $this->assertTrue($oType->getName(true) == '\Vendor\Namespace\Class');
            $this->assertTrue($oType->getNamespace() == 'Vendor\Namespace');
            $this->assertFalse($oType->isInGlobalNamespace());
            $oNewType = $oType->rewriteTypeInNamespace('\Test');
            $this->assertTrue($oNewType == '\Vendor\Test\Class');
            $oNewType = $oType->rewriteTypeInNamespace('\Test', false);
            $this->assertTrue($oNewType == '\Test\Class');
        }

        public function testType_NoNamespace() {
            $oType = Type::getTypeOf('Class');
            $this->assertTrue($oType->getFullName() == '\Class');
            $this->assertTrue($oType->__toString() == '\Class');
            $this->assertTrue($oType->getName() == 'Class');
//            $this->assertTrue($oType->getName(true) == '\Class'); // TODO błąd
//            $this->assertTrue($oType->getNamespace() == '\'); //TODO błąd
            $this->assertTrue($oType->isInGlobalNamespace());
            $oNewType = $oType->rewriteTypeInNamespace('\Test');
            $this->assertTrue($oNewType == '\Test\Class');
        }
    }

}



<?php
namespace AtomPiePhpUnitTest\Generi;

use Generi\Object;
use Generi\Type;

class XObject extends Object {

}

class YObject extends Object {

}

class ObjectTest extends \PHPUnit_Framework_TestCase {

    public function testObject_getType() {
        $oX = new XObject();
        $this->assertTrue($oX->getType() instanceof Type);
        $this->assertTrue($oX->getType()->__toString() == '\AtomPiePhpUnitTest\Generi\XObject');
    }

    public function testObject_Type() {
        $this->assertTrue(XObject::Type() instanceof Type);
        $this->assertTrue(XObject::Type() == '\AtomPiePhpUnitTest\Generi\XObject');
    }

}

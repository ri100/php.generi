<?php
namespace AtomPiePhpUnitTest\Generi;

use Generi\NameValuePair;

class NameValuePairTest extends \PHPUnit_Framework_TestCase {

    public function testKeyValue_Value() {
        $oKVObject = new NameValuePair('key','value');
        $this->assertTrue($oKVObject->getValue() === 'value');
    }

    public function testKeyValue_Key() {
        $oKVObject = new NameValuePair('key','value');
        $this->assertTrue($oKVObject->getName() === 'key');
    }

    public function testKeyValue_ToString() {
        $oKVObject = new NameValuePair('key','value');
        $this->assertTrue($oKVObject->__toString() == 'key=value');
    }

    public function testKeyValue_IsEmpty() {
        $oKVObject = new NameValuePair('key',null);
        $this->assertTrue($oKVObject->isEmpty());
        $this->assertFalse($oKVObject->hasValue());

    }

}

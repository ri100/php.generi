<?php
namespace AtomPiePhpUnitTest\Generi;

use Generi\StringConverter;

class StringConverterTest extends \PHPUnit_Framework_TestCase {
    public function testStringConverter_ToCamelCase() {
        $this->assertTrue('TestToCamelCase' == StringConverter::ConvertToCamelCase('test_to_camel_case'));
    }
    public function testStringConverter_ToUnderScore() {
        $this->assertTrue('test_to_camel_case' == StringConverter::ConvertToUnderscore('TestToCamelCase'));
    }
    public function testStringConverter_ToSpaceSeparated() {
        $this->assertTrue('Test to camel case' == StringConverter::SpaceSeparated('test_to_camel_case'));
    }
}

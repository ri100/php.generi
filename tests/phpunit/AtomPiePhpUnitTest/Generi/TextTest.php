<?php
namespace AtomPiePhpUnitTest\Generi {

    use Generi\Text;

    /**
     * Collection test case.
     */
    class TextTest extends \PHPUnit_Framework_TestCase {

        /**
         * Prepares the environment before running a test.
         */
        protected function setUp() {
            parent::setUp();
        }

        /**
         * Cleans up the environment after running a test.
         */
        protected function tearDown() {
            parent::tearDown();
        }

        public function testDeprefix() {
            $oString = new Text('aabbcc');
            $this->assertTrue('bbcc' == $oString->dePrefix('aa'));
            $this->assertTrue(false == $oString->dePrefix('bb'));
        }

        public function testEqual() {
            $oString = new Text('aabbcc');
            $this->assertTrue($oString->equal('aabbcc'));
            $this->assertFalse($oString->equal('xxx'));
        }

        public function testString_toCamelCase() {
            $oString = new Text('test_to_camel_case');
            $this->assertTrue('TestToCamelCase' == $oString->toCamelCase());
        }

        public function testString_toUnderScore() {
            $oString = new Text('TestToCamelCase');
            $this->assertTrue('test_to_camel_case' == $oString->toUnderscore());
        }

        public function testString_toSeparated() {
            $oString = new Text('test_to_camel_case');
            $this->assertTrue('Test to camel case' == $oString->toSpaceSeparated());
        }

        public function testString_toLowerCase() {
            $oString = new Text('TEST_to_camel_case');
            $this->assertTrue('test_to_camel_case' == $oString->toLowerCase());
        }

        public function testString_toUpperCase() {
            $oString = new Text('TEST_to_camel_case');
            $this->assertTrue('TEST_TO_CAMEL_CASE' == $oString->toUpperCase());
        }

        public function testString_hasString() {
            $oString = new Text('TEST_to_camel_case');
            $this->assertTrue($oString->has('TEST'));
        }

        public function testString_replace() {
            $oString = new Text('TEST_to_camel_case');
            $this->assertTrue($oString->replace('TEST','X') == 'X_to_camel_case');
        }

        public function testString_find() { // TODO check
            $oString = new Text('TEST_to_camel_case');
            echo  $oString->find('TEST');
        }

        public function testString_length() {
            $oString = new Text('12345');
            $this->assertTrue($oString->length() == 5);
        }

        public function testString_startsWith() {
            $oString = new Text('12345');
            $this->assertTrue($oString->startsWith('1'));
            $this->assertFalse($oString->startsWith('2'));
        }

        public function testString_prefix() {
            $oString = new Text('12345');
            $this->assertTrue($oString->prefix('a') == 'a12345');
        }
    }

}
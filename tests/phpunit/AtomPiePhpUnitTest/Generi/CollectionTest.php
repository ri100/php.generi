<?php
namespace AtomPiePhpUnitTest\Generi {

    use Generi\Boundary\IToArray;
    use Generi\Collection;

    /**
     * @property string Prop1
     * @property string Prop2
     * @property string Prop3
     * @property B B
     */
    class A extends \stdClass implements IToArray {

        /**
         * @return \ArrayObject
         */
        public function __toArray() {
            $oArray = new \ArrayObject();
            if (isset($this->Prop1))
                $oArray['Prop1'] = $this->Prop1;
            if (isset($this->Prop2))
                $oArray['Prop2'] = $this->Prop2;
            if (isset($this->Prop3))
                $oArray['Prop3'] = $this->Prop3;
            if (isset($this->B)) {
                if($this->B instanceof IToArray) {
                    $oArray['B'] = $this->B->__toArray()->getArrayCopy();
                } else {
                    $oArray['B'] = 'Error';
                }
            }

            return $oArray;
        }
    }

    /**
     * @property string Prop1
     * @property string Prop2
     * @property string Prop3
     * @property C C
     */
    class B extends \stdClass implements IToArray {
        /**
         * @return \ArrayObject
         */
        public function __toArray() {
            $oArray = new \ArrayObject();
            if(isset($this->Prop1))
                $oArray['Prop1'] = $this->Prop1;
            if(isset($this->Prop2))
                $oArray['Prop2'] = $this->Prop2;
            if(isset($this->Prop3))
                $oArray['Prop3'] = $this->Prop3;
            if(isset($this->C)) {
                if($this->C instanceof IToArray) {
                    $oArray['C'] = $this->C->__toArray()->getArrayCopy();
                } else {
                    $oArray['C'] = 'Error';
                }
            }
            return $oArray;
        }
    }

    /**
     * @property string Prop1
     * @property string Prop2
     * @property string Prop3
     */
    class C extends \stdClass implements IToArray {
        /**
         * @return \ArrayObject
         */
        public function __toArray() {
            $oArray = new \ArrayObject();
            if(isset($this->Prop1))
                $oArray['Prop1'] = $this->Prop1;
            if(isset($this->Prop2))
                $oArray['Prop2'] = $this->Prop2;
            if(isset($this->Prop3))
                $oArray['Prop3'] = $this->Prop3;
            return $oArray;
        }
    }

    /**
     * Collection test case.
     */
    class CollectionTest extends \PHPUnit_Framework_TestCase {

        /**
         * Prepares the environment before running a test.
         */
        protected function setUp() {
            parent::setUp();
        }

        /**
         * Cleans up the environment after running a test.
         */
        protected function tearDown() {
            parent::tearDown();
        }

        /**
         * Constructs the test case.
         */
        public function __construct() { }

        public function testSet() {
            $oCollection = new Collection();
            $oCollection['test'] = 1;
            $this->assertTrue($oCollection->has('test'));
            $this->assertTrue($oCollection['test'] == $oCollection->get('test'));
            $this->assertTrue($oCollection == '1');
        }

        public function testReference() {
            $oCollection = new Collection();
            $oCollection->add(new \stdClass(), 'fillWith');

            $this->assertTrue($oCollection->has('fillWith'));

            $oX1 = $oCollection->get('fillWith');
            /* @var $oX1 \stdClass */
            $aX = $oCollection->get(array('fillWith'));
            $oX2 = $aX['fillWith'];
            /* @var $oX2 \stdClass */

            $this->assertTrue($oX1 === $oX2);
            $oX1->a = 'abc';
            $this->assertTrue($oX2->a == 'abc');
            $this->assertTrue($oCollection->get('fillWith')->a == 'abc');
        }

        public function testHas() {
            $oCollection = new Collection();
            $oCollection->add(1, 'a');
            $oCollection->add(null, 'b');
            $oCollection->add(3, 'c');
            $oCollection->add(new \stdClass(), 'fillWith');

            $this->assertTrue($oCollection->has('a'));
            $this->assertTrue($oCollection->has('b'));
            $this->assertFalse($oCollection->has('d'));
            $this->assertTrue($oCollection->has(array('a', 'b')));
            $this->assertFalse($oCollection->has(array('a', 'd')));

            $oCollection = new Collection();
            $this->assertFalse(isset($oCollection['test']));
            $oCollection['test'] = 1;
            $this->assertTrue(isset($oCollection['test']));
        }

        public function testRemove() {

            $oCollection = new Collection();
            $this->assertFalse(isset($oCollection['test']));
            $oCollection['test'] = 1;
            $this->assertTrue(isset($oCollection['test']));
            unset($oCollection['test']);
            $this->assertFalse(isset($oCollection['test']));

            $oCollection = new Collection();
            $oCollection->add(1, 'a');
            $oCollection->remove('a');
            $this->assertFalse($oCollection->has('a'));

            /////////////////////////
            // Reference by copy

            // Make copy of collection
            $aAll = $oCollection->getAll();
            $this->assertTrue(empty($aAll));

            // Add new item to copy
            $aAll['b'] = 1;
            $aAll1 = $oCollection->getAll();

            // Still should be empty
            $this->assertTrue(empty($aAll1));

            /////////////////////////
            // Reference by pointer

            // Do NOT make copy of collection
            // jut return reference
            $aAll =& $oCollection->getAll();
            $this->assertTrue(empty($aAll));

            // Add new item to copy
            $aAll['c'] = 1;
            $aAll1 = $oCollection->getAll();

            // Still should NOT be empty
            $this->assertFalse(empty($aAll1));
            $this->assertFalse(isset($aAll1['b']));
            $this->assertTrue($aAll1['c'] == 1);

            // Remove all

            $oCollection->removeAll();
            $aAll = $oCollection->getAll();
            $this->assertTrue(empty($aAll));
        }

        public function testAddAll() {
            $aAll = array('a', 'b', 'c');
            $oCollection = new Collection();
            $oCollection->addAll($aAll);

            $aAll2 = $oCollection->getAll();
            $this->assertFalse(empty($aAll2));
        }

        public function testCount() {
            $aAll = array('a', 'b', 'c');
            $oCollection = new Collection();
            $oCollection->addAll($aAll);

            $this->assertTrue(count($aAll) == $oCollection->count());
        }

        public function testCurrent() {
            $aAll = array('a', 'b', 'c');
            $oCollection = new Collection();
            $oCollection->addAll($aAll);
            $this->assertTrue('a' == $oCollection->current());
            $this->assertTrue('a' == $oCollection[0]);
        }

        public function testNextAndRewind() {
            $aAll = array('a', 'b', 'c');
            $oCollection = new Collection();
            $oCollection->addAll($aAll);

            $oCollection->next();
            $this->assertTrue('b' == $oCollection->current());
            $oCollection->next();
            $this->assertTrue('c' == $oCollection->current());

            $oCollection->rewind();
            $this->assertTrue('a' == $oCollection->current());
        }

        public function testSingleAndEmpty() {
            $aAll = array('a', 'b', 'c');
            $oCollection = new Collection();
            $oCollection->addAll($aAll);

            $this->assertFalse($oCollection->isSingle());
            $oCollection->remove(1);
            $oCollection->remove(2);

            $this->assertTrue($oCollection->isSingle());

            $oCollection->remove(0);

            $this->assertTrue($oCollection->isEmpty());
        }


        public function testEmptyCollectionToArray() {
            $oCollection = new Collection();
            $this->assertTrue($oCollection->__toArray()->getArrayCopy() == array());
        }


        public function testCollectionConvert() {

            $oA = new Collection();
            $oA['Prop1'] = 'a';
            $oA['Prop2'] = 'aa';
            $oA['Prop3'] = 'aaa';

            $oB = new Collection();
            $oB['Prop1'] = 'b';
            $oB['Prop2'] = 'bb';
            $oB['Prop3'] = 'bbb';

            $oC = new Collection();
            $oC['Prop1'] = 'c';
            $oC['Prop2'] = 'cc';
            $oC['Prop3'] = 'ccc';

            $oCollection = new Collection();
            $oCollection->addAll(array($oA, $oB, $oC));
            /** @noinspection PhpUnusedParameterInspection */
            $oCollection->iterate(function ($sKey, $aItem) {
                /** @var Collection $aItem */
                $aItem['Prop1'] = '1';
                $aItem['Prop2'] = '2';
                $aItem['Prop3'] = '3';
                return $aItem->getAll();
            });
            foreach ($oCollection as $aItem) {
                $this->assertTrue(count($aItem) == 3);
                $this->assertTrue($aItem['Prop1'] == '1');
                $this->assertTrue($aItem['Prop2'] == '2');
                $this->assertTrue($aItem['Prop3'] == '3');
            }

        }

        public function testCollectionToArray() {

            // Simple array

            $aInData = array('a', 'b', 'c');
            $oCollection = new Collection();
            $oCollection->addAll($aInData);
            $this->assertTrue($oCollection->__toArray()->getArrayCopy() == $aInData);

            // Model in array

            $oA = new A();
            $oA->Prop1 = 'a';
            $oA->Prop2 = 'aa';
            $oA->Prop3 = 'aaa';

            $oB = new B();
            $oB->Prop1 = 'b';
            $oB->Prop2 = 'bb';
            $oB->Prop3 = 'bbb';

            $aInData = array(
                'a' => $oA,
                'b' => $oB,
                'c' => new C()
            );

            $aResult = array(
                'a' => array('Prop1' => 'a', 'Prop2' => 'aa', 'Prop3' => 'aaa'),
                'b' => array('Prop1' => 'b', 'Prop2' => 'bb', 'Prop3' => 'bbb'),
                'c' => array()
            );
            $oCollection = new Collection();
            $oCollection->addAll($aInData);
            $this->assertTrue($oCollection->__toArray()->getArrayCopy() == $aResult);

            // Model in array with model

            $oB = new B();
            $oB->Prop1 = 'b';
            $oB->Prop2 = 'bb';
            $oB->Prop3 = 'bbb';

            $oA = new A();
            $oA->Prop1 = 'a';
            $oA->Prop2 = 'aa';
            $oA->B = $oB;


            $aInData = array(
                'a' => $oA,
                'c' => new C()
            );

            $aResult = array(
                'a' => array(
                    'Prop1' => 'a',
                    'Prop2' => 'aa',
                    'B' => array('Prop1' => 'b', 'Prop2' => 'bb', 'Prop3' => 'bbb')
                ),
                'c' => array()
            );
            $oCollection = new Collection();
            $oCollection->addAll($aInData);
            $this->assertTrue($oCollection->__toArray()->getArrayCopy() == $aResult);

            // Model in array with model/model

            $oC = new C();
            $oC->Prop1 = 'c';
            $oC->Prop2 = 'cc';
            $oC->Prop3 = 'ccc';

            $oB = new B();
            $oB->Prop1 = 'b';
            $oB->Prop2 = 'bb';
            $oB->C = $oC;

            $oA = new A();
            $oA->Prop1 = 'a';
            $oA->Prop2 = 'aa';
            $oA->B = $oB;


            $aInData = array(
                'a' => $oA
            );

            $aResult = array(
                'a' => array(
                    'Prop1' => 'a',
                    'Prop2' => 'aa',
                    'B' => array(
                        'Prop1' => 'b',
                        'Prop2' => 'bb',
                        'C' => array(
                            'Prop1' => 'c',
                            'Prop2' => 'cc',
                            'Prop3' => 'ccc',
                        ))
                )
            );
            $oCollection = new Collection();
            $oCollection->addAll($aInData);
            $this->assertTrue($oCollection->__toArray()->getArrayCopy() == $aResult);

            // Collection in Collection

            $oC = new Collection();
            $oC['Prop1'] = 'c';
            $oC['Prop2'] = 'cc';
            $oC['Prop3'] = 'ccc';

            $oB = new Collection();
            $oB['Prop1'] = 'b';
            $oB['Prop2'] = 'bb';
            $oB['C'] = $oC;

            $oA = new Collection();
            $oA['Prop1'] = 'a';
            $oA['Prop2'] = 'aa';
            $oA['B'] = $oB;


            $aInData = array(
                'a' => $oA
            );

            $aResult = array(
                'a' => array(
                    'Prop1' => 'a',
                    'Prop2' => 'aa',
                    'B' => array(
                        'Prop1' => 'b',
                        'Prop2' => 'bb',
                        'C' => array(
                            'Prop1' => 'c',
                            'Prop2' => 'cc',
                            'Prop3' => 'ccc',
                        ))
                )
            );
            $oCollection = new Collection();
            $oCollection->addAll($aInData);
            $this->assertTrue($oCollection->__toArray()->getArrayCopy() == $aResult);
        }

        public function testIterate() {

            $oA = array();
            $oA['A::Prop1'] = 'a';
            $oA['A::Prop2'] = 'aa';
            $oA['A::Prop3'] = 'aaa';

            $oB = array();
            $oB['A::Prop1'] = 'b';
            $oB['A::Prop2'] = 'bb';
            $oB['A::Prop3'] = 'bbb';

            $oC = array();
            $oC['A::Prop1'] = 'c';
            $oC['A::Prop2'] = 'cc';
            $oC['A::Prop3'] = 'ccc';

            $aData = array($oA, $oB, $oC);
            $oCollection = new Collection($aData);

            $iRecord = 0;
            $aArray = array();

            /** @noinspection PhpUnusedParameterInspection */
            $oCollection->iterate(function ($sKey, $mItem) use (&$aArray, &$iRecord) {

                if (is_array($mItem)) {
                    foreach ($mItem as $sPropertyKey => $mProperty) {
                        $aArray[$iRecord][$sPropertyKey] = $mProperty;
                    }
                    $iRecord++;
                }

            });
            // All properties from all object are merged into one array.

            $this->assertTrue(count($aArray) == 3);
            $this->assertTrue($aArray[2]['A::Prop1'] == 'c');
            $this->assertTrue($aArray[2]['A::Prop2'] == 'cc');
            $this->assertTrue($aArray[2]['A::Prop3'] == 'ccc');

            // Old collection stays the same

            $this->assertTrue($oCollection[0] == $oA);
            $this->assertTrue($oCollection[1] == $oB);
            $this->assertTrue($oCollection[2] == $oC);
        }

        public function testIterateWithReference() {

            $oA = array();
            $oA['A::Prop1'] = 'a';
            $oA['A::Prop2'] = 'aa';
            $oA['A::Prop3'] = 'aaa';

            $oB = array();
            $oB['A::Prop1'] = 'b';
            $oB['A::Prop2'] = 'bb';
            $oB['A::Prop3'] = 'bbb';

            $oC = array();
            $oC['A::Prop1'] = 'c';
            $oC['A::Prop2'] = 'cc';
            $oC['A::Prop3'] = 'ccc';

            $aData = array($oA, $oB, $oC);
            $oCollection = new Collection($aData);

            /** @noinspection PhpUnusedParameterInspection */
            $oCollection->iterate(function ($sKey, $mItem) {
                return 1;
            });

            $this->assertTrue($oCollection[0] == 1);
            $this->assertTrue($oCollection[1] == 1);
            $this->assertTrue($oCollection[2] == 1);
        }

    }
}
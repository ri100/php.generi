<?php
namespace AtomPiePhpUnitTest\Generi;

use Generi\Structure;

class StructureTest extends \PHPUnit_Framework_TestCase {

    public function testToArray() {
        $oStruct = new Structure();
        $oStruct->FirstSecond = 1;
        $aArray = $oStruct->__toArray(true);
        $this->assertTrue(isset($aArray['first_second']));
        $this->assertTrue(1 == $aArray['first_second']);

        $oStruct->SubNode = new Structure();

        $oStruct->SubNode->SubName = 2;

        $aArray = $oStruct->__toArray(true);
        $this->assertTrue(isset($aArray['sub_node']));
        $this->assertTrue(isset($aArray['sub_node']['sub_name']));

        $aArray = $oStruct->__toArray(false);
        $this->assertTrue(isset($aArray['SubNode']));
        $this->assertTrue(isset($aArray['SubNode']['SubName']));
    }

    public function testToJson() {
        $oStruct = new Structure();
        $oStruct->Name = 1;
        $this->assertTrue('{"Name":1}' == $oStruct->__toJson());

        $oStruct->Sub = new Structure();

        $oStruct->Sub->SubName = 2;

        $this->assertTrue('{"Name":1,"Sub":{"SubName":2}}' == $oStruct->__toJson());

        $this->assertTrue(isset($oStruct->Sub));
        unset($oStruct->Sub);

        $this->assertTrue('{"Name":1}' == $oStruct->__toJson());

    }

}
 
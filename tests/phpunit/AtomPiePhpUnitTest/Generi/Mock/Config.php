<?php
namespace AtomPiePhpUnitTest\Generi\Mock {

    use Generi\Boundary\IConstrainValueType;
    use Generi\ReadOnlyDataObject;

    /**
     * Class Config
     * @package GeneriTest\Resource
     * @property \stdClass $test
     */
    class Config extends ReadOnlyDataObject implements IConstrainValueType {

        public function __constrainSpec() {
            return array(
                'test' => '\stdClass'
            );
        }
    }

}

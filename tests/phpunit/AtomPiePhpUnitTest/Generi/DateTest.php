<?php
namespace AtomPiePhpUnitTest\Generi;

use Generi\Date;

class DateTest extends \PHPUnit_Framework_TestCase {
    public function testDate_ToString() {
        $sDate = '2015-02-26 12:34:01';
        $oDate = new Date($sDate);
        $this->assertTrue($oDate->__toString() == $sDate);
    }

    public function testDate_TimeOnly() {
        $sDate = '2015-02-26 12:34:01';
        $this->assertTrue( Date::getDateOnly($sDate) == '2015/02/26');
    }

    public function testDate_TimeStamp() {
        $sDate = '2015-02-26 12:34:01';
        $oDate = new Date($sDate);
        $this->assertTrue($oDate->getTimeStamp() == strtotime($sDate));

        $oDate = new Date(strtotime($sDate));
        $this->assertTrue($oDate->getTimeStamp() == strtotime($sDate));
    }

    public function testDate_IsLargerThen() {
        $sDate = '2015-02-26 12:34:01';
        $sLaterDate = '2015-02-26 12:34:02';
        $sEarlierDate = '2015-02-26 12:34:00';
        $oDate = new Date($sDate);
        $this->assertTrue($oDate->isLaterThen(new Date($sEarlierDate)));
        $this->assertFalse($oDate->isLaterThen(new Date($sLaterDate)));
    }

    public function testDate_SecondsTo() {
        $sDate = '2015-02-26 12:34:01';
        $sLaterDate = '2015-02-26 12:34:02';
        $sEarlierDate = '2015-02-26 12:34:00';
        $oDate = new Date($sDate);
        $this->assertTrue($oDate->getSecondsTo(new Date($sEarlierDate)) == -1);
        $this->assertFalse($oDate->isLaterThen(new Date($sLaterDate)) == 1);
    }
}

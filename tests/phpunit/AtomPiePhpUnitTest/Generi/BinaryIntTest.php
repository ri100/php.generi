<?php
namespace AtomPiePhpUnitTest\Generi;

use Generi\BinaryInt;

class BinaryIntTest extends \PHPUnit_Framework_TestCase {

    public function testBinaryInt() {
        $oInt = new BinaryInt(10);
        $this->assertTrue($oInt->getValue() == 10);
        $oInt->setValue(2);
        $this->assertTrue($oInt->getValue() == 2);
    }

    public function testBinaryInt_hasBit() {
        $oInt = new BinaryInt(5);
        $this->assertTrue($oInt->hasBit(1));
        $this->assertFalse($oInt->hasBit(2));
        $this->assertTrue($oInt->hasBit(4));
    }

    public function testBinaryInt_hasBit_Exception() {
        $oInt = new BinaryInt(3);
        $this->setExpectedException('\Exception');
        $oInt->hasBit('4');
    }

}
